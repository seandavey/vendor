# vendor

third-party jars needed for Mosaic or core based sites

app.jar
https://gitlab.com/seandavey/core/-/blob/master/app/Valve.java

biweekly is an iCalendar library written in Java.
https://github.com/mangstadt/biweekly
https://github.com/mangstadt/vinnie

The Jakarta Mail API provides a platform-independent and protocol-independent framework to build mail and messaging applications.
https://projects.eclipse.org/projects/ee4j.mail

jsoup is a Java library for working with real-world HTML.
https://jsoup.org

minimal-json, a fast and minimal JSON parser and writer for Java.
https://github.com/ralfstx/minimal-json

PostgreSQL JDBC Driver
https://jdbc.postgresql.org
